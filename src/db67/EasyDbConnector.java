package db67;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class EasyDbConnector {

    public static void main(String args[]) throws Exception {
        int buttonChoice = 0;
        // load the jdbc driver
        DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
        System.out.println("Waiting for Login..");

        // show login dialog and wait for a correct database login
        LoginDialogView lg = new LoginDialogView();

        // get the database connection from the login dialog
        Connection conn = lg.getConn();
        Statement stmt1 = conn.createStatement();
        ResultSet rset1 = stmt1.executeQuery("select table_name from user_tables");
        ArrayList<String> list = new ArrayList<String>();
        while (rset1.next()) {
            list.add(rset1.getString("table_name"));
        }
        lg.initialize(list);
        lg.eingabe();

        Statement stmt = null;
        // execute a query
        buttonChoice = DataBaseChoice.getButtonChoice();
        do {
            // build a statement
            if (1 <= DataBaseChoice.getButtonChoice() && DataBaseChoice.getButtonChoice() <= list.size()) {
                buttonChoice = DataBaseChoice.getButtonChoice();
                stmt = conn.createStatement();
            }
            if (0 <= buttonChoice && buttonChoice <= list.size()) {
                ResultSet rset = stmt.executeQuery("select * from " + list.get(buttonChoice - 1));
                ResultSetMetaData rsmd = rset.getMetaData();
                int count = rsmd.getColumnCount();
                System.out.println(list.get(buttonChoice - 1));
                System.out.println("================================================================================================");
                print(rset, rsmd, count);
                lg.eingabe();
                buttonChoice = 9;
                DataBaseChoice.setButtonChoice(0);
            }
        } while (true);
    }

    public static void print(ResultSet rset, ResultSetMetaData rsmd, int count) throws SQLException {
        for (int i = 1; i < count + 1; i++) {
            System.out.print(rsmd.getColumnName(i) + "    ");
        }
        System.out.println("");

        while (rset.next()) {
            for (int i = 1; i < count + 1; i++) {
                System.out.print(rset.getString(rsmd.getColumnName(i)) + "    ");
            }
            System.out.println("");
        }
        System.out.println("================================================================================================\n\n");
    }
}
