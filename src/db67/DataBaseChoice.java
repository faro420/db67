package db67;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class DataBaseChoice extends JFrame implements ActionListener {

    private static final long serialVersionUID = 1L;

    JPanel panel;
    ArrayList<JButton> list = new ArrayList<JButton>();
    static int buttonChoice;

    DataBaseChoice(ArrayList<String> names) {
        super("DataBaseChoice");
        System.out.println("Choose a Database...");
        WindowListener l = new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                setVisible(false);
                dispose();
                System.exit(0);
            }
        };
        addWindowListener(l);
        setSize(300, 640);
        setLocation(200, 200);
        addControls(names);
        setVisible(true);
    }

    private void addControls(ArrayList<String> names) {

        Container contentPane = this.getContentPane();
        DBButtonListener bl = new DBButtonListener();
        JPanel panel = new JPanel();
        for (int i = 0; i < names.size(); i++) {
            list.add(new JButton(names.get(i)));
            list.get(i).setBounds(15, 20 + 50 * i, 150, 40);
            list.get(i).addActionListener(bl);
            contentPane.add(list.get(i));
            list.get(i).setVisible(true);
        }
        contentPane.add(panel);
    }

    private class DBButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            Object dummy = e.getSource();
            for (int i = 0; i < list.size(); i++) {
                if (dummy == list.get(i)) {
                    buttonChoice = i + 1;
                    i = list.size();
                }
            }
        }
    }

    public static int getButtonChoice() {
        return buttonChoice;
    }

    public static void setButtonChoice(int x) {
        buttonChoice = x;
    }

    public void actionPerformed(ActionEvent arg0) {
    }
}
